Empire is a board game of colonization, negotiation, and competition.

If you're interested in contacting the designers, you can reach them at [stephensongames.com](http://stephensongames.com).
