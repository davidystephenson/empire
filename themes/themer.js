var fs = require('fs');

var Setting = function(names) {
  var setting = JSON.parse(fs.readFileSync('./eighteenthCentury.json', 'utf8'));

  for (var name in names) {
    if (names[name] && names[name] !== '') setting[name] = names[name];
  }

  setting.reports = [
    'Combining a ' + setting.territory + ' and a ' + setting.population + ' creates a ' + setting.good,
    'Combining a ' + setting.good + ' and a ' + setting.population + ' creates a ' + setting.supporter,
    'Acquiring a ' + setting.bond + ' gives a burst of ' + setting.gold + ' but costs ' + setting.gold + ' over time',
    'Not paying for a ' + setting.bond + ' creates a ' + setting.revolt,
    'Outputing a ' + setting.good + ' gives you ' + setting.gold + ', but the more outputted ' + setting.good + ', the less ' + setting.gold + ' each gives',
    'A ' + setting.conflict + ' is competed for with ' + setting.gold,
    'Strategic choices are made with ' + setting.selection,
    'Having ' + setting.diplomacy + ' can overcome a lack of ' + setting.gold,
  ]

  setting.report = function () {
    console.log('\n' + setting.title + ' (' + setting.name + ')');
    console.log('--------------------------');
    for (var i = 0; i < setting.reports.length; i ++) {
      console.log(setting.reports[i]);
    }
  }

  return setting;
}

var themes = [
  './eighteenthCentury.json',
  './wasteland.json'
];

for (var i = 0; i < themes.length; i ++) {
  var theme = JSON.parse(fs.readFileSync(themes[i], 'utf8'));
  var setting = Setting(theme);
  setting.report();
}
