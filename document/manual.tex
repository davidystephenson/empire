\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[letterpaper, left=1.5cm, right=1.5cm, top=2cm, bottom=1.75cm]{geometry}
\linespread{1}
\usepackage{anyfontsize}
\usepackage{array}
\usepackage{float}
\usepackage[small,bf]{caption}
\usepackage{setspace}
\usepackage[unicode=true]{hyperref}
\usepackage{mdframed}
\usepackage{fancyhdr}
\usepackage{titlesec}
\usepackage{paralist}
\usepackage{enumitem}
\setitemize{noitemsep,topsep=2mm,parsep=0mm,partopsep=2mm}

\titlespacing\section{0pt}{2pt}{0pt}
\titlespacing\subsection{0pt}{1pt}{0pt}
\titlespacing\subsubsection{0pt}{0pt}{0pt}

\setlength{\parindent}{0cm}
\setlength{\parskip}{3mm}
\setlength{\baselineskip}{12pt}
\setlength{\itemsep}{0pt}
\setlength{\plitemsep}{2mm}

%\makeatother
\newcommand{\vtwofive}{\rule{0pt}{2.5ex}}
\newcommand{\vfour}{\rule{0pt}{4ex}}

\pagestyle{fancy}
\renewcommand{\subsectionmark}[1]{\markright{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead[L]{\slshape \rightmark}
\fancyhead[R]{\thepage}
\fancyfoot[C]{www.stephensongames.com}

\setlength{\headheight}{15pt} 

\begin{document}

\title{
  \vspace*{2.4in}
  \fontsize{100}{28} \selectfont 
  \hspace{0mm} EMPIRE \\
  \fontsize{20}{28} \selectfont 2-6 Players | 30-60 minutes | Ages 12+
}

\author{
  \fontsize{30}{28} \selectfont 
  \hspace{0mm} Stephenson Brothers Games
}
\date{}
\maketitle
\thispagestyle{empty}
\pagebreak

\linespread{1}
\fontsize{12}{12} \selectfont
\setcounter{page}{1}

\section{Introduction}

In Empire, players play as colonial European nations.
The goal of the game is to have the most support; you want your population behind you when the next invasion, famine, or revolution happens!
Players gain support by supplying their people with goods, which are produced when their people work in their territories.
Players can gain people, territories, goods, and more by winning wars.
To win wars, players must raise armies with gold gained by selling goods to the market or taking out bonds.
At the end of the game, the player with the most support wins.

\section{Components}

\textbf{150 Support Cards} \\
Support cards represent both a nation's happy citizens and its rebellious factions.
There are two types of support cards: supporters (100) and revolts (50).
Supporters increase a player's support, while revolts decrease a player's support.

\textbf{160 Holding Chits} \\
Holding chits represent the property in a nation's sphere of influence.
There are three types of holdings: goods (60), territories (70), and bonds (30).

\emph{Goods} represent all the material things that citizens desire, from wheat and lumber to spices, tea, and whiskey.
Goods can be given to people to gain their support or sold to the market to earn gold.

\emph{Territories} represent the land a nation controls, both domestic provinces and foreign colonies.
When people migrate to territories, they work the land to produce goods.

\emph{Bonds} represent a nation's debt.
If nations fail to make their bond payments, their people will rebel.

\textbf{75 People Figures} \\
People figures represent a nation's population.
People can work on territories to produce goods or live lives of luxury by consuming goods.

\textbf{75 Gold Coins} \\
A nation's wealth is measured in gold.
Gold is used to raise armies and win wars over resources.

\textbf{60 Selection Cards} \\
Each player has a hand of selection cards numbered 0 to 9.
Players use their selection cards to claim nations, sell goods, and raise armies.

\textbf{10 Nation Cards} \\
Nation cards represent colonial European nations.
Each nation card lists a unique set of starting resources, a special power that only its owner can use, and a unique number from 0 to 9. 

\textbf{10 National Flags} \\
Each player claims a national flag at the beginning of the game which they use to win wars.

\textbf{10 War Tiles} \\
Wars represent opportunities for expansion around the world.
Each war tile lists the four resources that will be provided to its winner.
War tiles have a multicolored "active" side and a black "inactive" side. 

\textbf{1 War Bag} \\
The war bag is used to shuffle the war tiles at the beginning of the game.

\textbf{1 Diplomacy Token} \\
Whoever owns the diplomacy token is the diplomat. The diplomat breaks stalemates during war.

\textbf{1 Market Board} \\
The market mat shows 9 ports, each representing a different location around the world where goods can be sold.
Each round, players sell goods to the market for gold.
The more goods the players sell, the less gold each good is worth.

\section{Setup}

\begin{enumerate}
    \item \textbf{The Supply} - Separate the support cards, gold coins, people figures, and holding chits into stacks by type in the center of the play area to form the supply.
    
    \item \textbf{The Market} - Place the market board in the center of the play area.
    Indicate which ports on the market mat are unavailable with the current number of players by covering them with bonds.
    
    \item \textbf{War Tiles} - Return any war tiles that are not available with the current number of players to the box.
    
	Shuffle the remaining war tiles in the war bag and randomly draw a number of war tiles equal to the 
number of players and place them in a row, active side up.
    Then, randomly draw three more war tiles and add them to the end of the row, active side down.

    Show the one war tile left in the bag to all the players and then return the war bag and remaining war tile to the box. 
\end{enumerate}

\section{Drafting Nations}

At the beginning of the game, each player drafts a nation. A player's nation determines their starting resources and grants them a special power while they hold the nation's card.

\begin{enumerate}
    \item \textbf{Nation Cards} - Lay out all 10 nation cards in a face-up row.
    Each player places their selection card with the number of the nation they want to claim face down. 

    Then, all players simultaneously reveal their cards and each player who claimed a nation that no one else did takes the nation card they claimed, as well as the corresponding national flag.

    If multiple players claimed the same nation card, place one revolt beside each nation card that multiple players claimed.
    Those players participate in a second round of the selection process. Repeat this process (placing revolts as necessary) until each player has claimed a different nation card.
    Return all unclaimed nation cards and national flags to the box.

    \vspace{3mm}
    \begin{mdframed}
        \textbf{Example A}: Diana, Mary, and Florence are selecting their nation cards.
        Each player places in front of them, face down, their selection card with the number of the nation they want to claim.
        Diana wants Portugal and places her "2" selection card face down in front of her.
        Mary and Florence both want Austria and each places their "9" selection card face down in front of them.
        
        When all three players have placed their cards face down, all players reveal their selections. Diana selected a nation that no other player did, so she claims the Portugal nation card.
        Mary and Florence selected the same nation, so a revolt card is placed next to the Austria nation card and both players again place in front of them, face down, their selection card with the number of the nation they want to claim.
        
        Mary still wishes to claim Austria, and again places her "9" selection card face down in front of her.
        Florence decides to go for France, and places her "6" selection card face down in front of her. Mary and Florence then reveal their new selections.
        Since both players selected a nation no other player did, each claims the nation they selected. Mary also takes the revolt that was placed next to Austria.
    \end{mdframed}

    \item \textbf{Starting Resources} - Give each player five gold and the resources listed in the corresponding section of their nation card.

    \emph{Note: Players must keep their gold coins, holdings chits, and people figures in their tableau so that they are visible to the other players at all times.}

    \item \textbf{Diplomacy Token} - Give the diplomacy token to the player whose nation card has the lowest number.
\end{enumerate}

\section{Rounds}

Empire takes place over five rounds of gameplay.
Each of the first four rounds has four phases: Build, Develop, Market, and War.
In the fifth and final round, only the first two phases occur.

In each phase, the steps must be performed in order.
There are no individual player turns; all players walk through the steps of each phase simultaneously.

\subsection{Build Phase}
During the build phase, players use their resources to build institutions. The build phase represents the people of each nation making choices about their lives.

An \emph{institution} is a stack of two resources.
There are three types of institutions: banks, luxuries, and industries, each requiring a specific combination of resources.
Each resource may only be used in one institution each round. 

Institutions are built in a specific order: banks > luxuries > industries.
Each player must build as many of each institution as they can before building any of the next type of institution.

\begin{enumerate}
    \item Build as many \emph{banks} as you can.
    To build a bank, stack a gold on a bond.
    
    \item Build as many \emph{luxuries} as you can.
    To build a luxury, stack a people figure on a good.
    
    \item Build as many \emph{industries} as you can.
    To build an industry, stack a people figure on a territory.
\end{enumerate}

\emph{Tip: Since luxuries and industries both require people, you will have to manage your people carefully if you want to be able to build industries.}

\begin{mdframed}
    \textbf{Example B}: At the beginning of the build phase, Omar has 5 gold, 3 bonds, 2 goods, 5 people, and 4 territories.

    First, Omar builds as many banks as he can.
    Omar has 3 bonds and 5 gold available, so he places 1 gold on each of his bonds, building 3 banks.
    
    Then, Omar builds as many luxuries as he can.
    Omar has 2 goods and 5 people available, so he places 1 people on each of his goods, building 2 luxuries.
    
    Finally, Omar builds as many industries as he can.
    Omar has 4 territories and 3 people available, so he places each of his remaining people on a different 1 of his territories, building 3 industries.
\end{mdframed}

\subsection{Develop Phase}

During the develop phase, players' resources and institutions activate, affecting their goods, gold, and levels of support.

\begin{enumerate}
    \item \textbf{Unemployed People} - If a player has more than one people not used in a luxury or industry, they take one revolt.
    
    \item \textbf{Unpaid Bonds} - For each of their bonds not in a bank, a player takes one revolt.
    
    \item \textbf{Banks} - For each bank a player has, they must pay the gold from that bank to the supply.
    
    \item \textbf{Luxuries} - For each luxury a player has, they must pay the good from that luxury to the supply and take one supporter.
    
    \item \textbf{Industries} - For each industry a player has, they take one good.
\end{enumerate}

\begin{mdframed}
    \textbf{Example C}: During the build phase, Aiko built 3 banks, 3 luxuries, and 2 industries.
    4 of her people were not used in an industry or luxury.
    Aiko also was not able to pay for all of her bonds, so 2 of her bonds are not in banks.
    

    At the beginning of the develop phase, Aiko takes 1 revolt because she has more than 1 unemployed people.
    Then, Aiko takes 2 revolts for her 2 unpaid bonds.
    Next, Aiko pays the gold from each of her banks to the supply.
    Then, Aiko pays the good from each of her 3 luxuries to the supply and takes 3 supporters.
    Finally, Aiko takes 2 goods from the supply for her 2 industries.
\end{mdframed}


\subsection{Market Phase}

During the market phase, players sell their goods for gold.
The number of goods being collectively sold by the players will determine the price of goods for the round.

\begin{enumerate}
    \item Each player decides the number of goods they want to sell, and places the corresponding selection card face down.
    
    \item All players simultaneously reveal their chosen selection cards, indicating how many goods they want to sell.
    
    \item Each player delivers the goods they are selling to the market.
    (If a player has fewer goods than the number indicated by their selection card, they just deliver as many as they have.)
    Each good is placed on a different empty port, beginning with the available port showing the highest price and continuing in order of descending value.
    
    \item For each good they sold, each player takes gold equal to the price of goods this round, which is determined by the number showing on the highest remaining available port.
    If all ports are full (due to players delivering a number of goods equal to or greater than the number of available ports), the price is 0.
    
    \item All goods delivered to the market are then returned to the supply.
\end{enumerate}

\begin{mdframed}
    \textbf{Example D}: It's a 4-player game, so the 2 highest-numbered ports were blocked with bonds during setup.
    In the market phase, Courtney decides to sell 3 goods, David decides to sell 1 good, Amanda decides to sell 0 goods, and Daniel decides to sell 1 good.
    
    All players deliver their goods to the market, covering up the 5 highest available ports (i.e., ports 3-7).
    The highest remaining available port is port 2, so the price of goods is 2 gold each.
    
    Courtney sold 3 goods, so she takes 6 gold.
    David sold 1 good, so he takes 2 gold.
    Amanda sold 0 goods, so she takes 0 gold.
    Daniel sold 1 good, so he takes 2 gold.
\end{mdframed}

\subsection{War Phase}

During the war phase, players win wars by paying gold to raise armies.
Different wars provide different combinations of resources when won, so raising more armies than your opponents is crucial so that you can choose which war you want to win. 

\begin{enumerate}
    \item Each player decides the number of armies they want to raise, and places the corresponding selection card face down.
    
    \item All players simultaneously reveal their chosen selection cards, indicating how many armies they want to raise.
    
    \item Each player then pays one gold for each army they want to raise.
    If a player does not have enough gold, they only raise as many armies as they can pay for.
    
    \item If one player raised fewer armies than every other player, they take the diplomacy token, becoming the new diplomat.
    If there is a tie for fewest armies, the current diplomat decides which of the tied players takes the diplomacy token.
    The diplomat may not select themself.
    
    \item In order from most armies to least armies, each player who raised at least one army selects one war to win, placing their flag on an active war tile that no other player has won this round and taking the listed resources.
    For each bond they take, they also take five gold.
    If multiple players raised exactly the same number of armies, the diplomat decides the order in which the tied players win wars.
    
    \item Flip the first active war tile face down (i.e., inactive side up), and flip the next inactive tile so that it is active side up.
\end{enumerate}

\begin{mdframed}
    \textbf{Example E}: Franklin, Winston, Joseph, and Charles are raising armies for war.
    Joseph is holding the diplomacy token.
    It's a 4-player game, so 4 wars are active.
    
    Franklin secretly decides to raise 4 armies.
    Winston also decides to raise 4 armies.
    Joseph decides to raise 6 armies, and Charles decides to raise 0 armies.
    
    All players then reveal their selections.
    Franklin and Winston each pay 4 gold, and Joseph pays 6 gold.
    Charles raised fewer armies than every other player, so he takes the diplomacy token from Joseph and becomes the new diplomat.
    
    Joseph raised the most armies so he goes first and places his national flag on any of the 4 active war tiles and takes the listed resources.
    
    Franklin and Winston each raised the same number of armies, so Charles, as the diplomat, decides which of them wins a war first.
    After some negotiation, Charles decides to let Winston go first.
    Winston places his national flag on any of the unclaimed active war tiles and takes the listed resources.
    Then Franklin does the same.
    
    Charles, who raised no armies, does not win a war this round.
\end{mdframed}

\section{Trading}

At any time during a round, players may freely trade any of the items in their tableaus, besides their national flags.
It is allowed to give items away for nothing, trade one type of item for another, trade unequal numbers of items, and even make trades involving promises of future favors (though such promises are non-binding).

Players may not trade an item while it is being used in an institution during the build or develop phases, but otherwise may make trades at any time during the game.

All trading, negotiation, and communication must be public.

\section{National Powers}

Players may (but are never required to) use the powers on their nation cards once each round.
Each nation card describes when the power can be used, a condition that must be satisfied for the power to be used, and the effect the power has when used.

Each power may only be used once each round, and players must announce a power's effect when they use it.
If a player acquires a nation card from another player (in a trade) after its ability has already been used in a given round, they may not use the ability again until the next round.

\section{Ending the Game}

During the war phase of the fourth round, the last war tile will be active, so no further tiles can be flipped.
In the fifth and final round, play through only the first two phases.
After the develop phase in the fifth round, the game ends.

The player with the most support wins!
A player's support is equal to their supporters minus their revolts.
If there is a tie for the most support, the tied player with the most remaining goods wins.
If there is still a tie, the tied player with the most gold wins.
If there is still a tie, the tied player with the highest numbered national flag wins.

\section{Introductory Variant}

You may wish to play without the nation cards for your first game.
To do so, incorporate the following rule changes:
Ignore the Drafting Nations instructions.
Instead, each player simply takes a national flag of their choice; then, return all national flags that were not selected to the box.
All players start the game with the same resources: four territories, four people, one good, and one bond.
The diplomacy token is given to the youngest player at the start of the game.

\section{Advanced Selection Variant}

Advanced players who want more negotiation and competitive balance can use the advanced nation selection variant. 

During nation selection, when players reveal their selections, if any players claim the same nation, none of the players take the nation they claimed.
Instead, one revolt is placed by each nation that multiple players claimed, and all players take their selection cards back into their hands and select again.
Repeat the selection process (placing revolts as necessary) until all players simultaneously claim a nation that no one else did.

\end{document}
